
# Rookmotion API v2.0.0

Collection of public methods for the RookMotion API.


## Indices

* [Rankings](#rankings)

  * [Retrive ranking](#1-retrive-ranking)

* [Rewards](#rewards)

  * [Add rewards to user](#1-add-rewards-to-user)
  * [Remove rewards from user](#2-remove-rewards-from-user)
  * [Retrive rewards of user](#3-retrive-rewards-of-user)

* [Sensors](#sensors)

  * [Add sensor to user](#1-add-sensor-to-user)
  * [Remove sensor from user](#2-remove-sensor-from-user)
  * [Retrive sensors from user](#3-retrive-sensors-from-user)
  * [Update sensor to user](#4-update-sensor-to-user)

* [Server](#server)

  * [Retrive server datetime](#1-retrive-server-datetime)

* [Trainings](#trainings)

  * [Add summaries type](#1-add-summaries-type)
  * [Add training to user](#2-add-training-to-user)
  * [Add training type](#3-add-training-type)
  * [Remove summaries type](#4-remove-summaries-type)
  * [Remove training from user](#5-remove-training-from-user)
  * [Remove training type](#6-remove-training-type)
  * [Retrieve training information of user](#7-retrieve-training-information-of-user)
  * [Retrive summaries types](#8-retrive-summaries-types)
  * [Retrive training state of user](#9-retrive-training-state-of-user)
  * [Retrive training types](#10-retrive-training-types)
  * [Retrive trainings of user](#11-retrive-trainings-of-user)
  * [Update summaries type](#12-update-summaries-type)
  * [Update training type](#13-update-training-type)

* [Users](#users)

  * [Add user physiological variables](#1-add-user-physiological-variables)
  * [Add user to RookMotion](#2-add-user-to-rookmotion)
  * [Remove user for RookMotion](#3-remove-user-for-rookmotion)
  * [Retrieve user physiological variables](#4-retrieve-user-physiological-variables)
  * [Retrieve user uuid](#5-retrieve-user-uuid)
  * [Retrive client users](#6-retrive-client-users)
  * [Retrive user indexes](#7-retrive-user-indexes)
  * [Retrive user information](#8-retrive-user-information)
  * [Retrive user statistics](#9-retrive-user-statistics)
  * [Retrive user status](#10-retrive-user-status)
  * [Set user membership](#11-set-user-membership)
  * [Update user information](#12-update-user-information)


--------


## Rankings
In this section you will find a list of the resources available in the RookMotion API to obtain user rewards.



### 1. Retrive ranking



***Endpoint:***

```bash
Method: POST
Type: RAW
URL: https://api.rookmotion.com/v2/rankings/client/users
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| ranking_type_uuid | RankingTypeUuid | Optional, initial end of period |



***Body:***

```js        
{
    "users_limit": 10,
    "from_date": "2020-12-10",
    "to_date": "2020-12-20",
    "users_order": "ascending",
    "criteria": {
        "sex": "male",
        "coverage": "center",
        "age": 35
    }
}
```



***More example Requests/Responses:***


##### I. Example Request: User ranking in the center


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| ranking_type_uuid | RankingTypeUuid | Unique ranking identifier for Rook Motion |



***Body:***

```js        
{
    "users_limit": 10,
    "from_date": "2020-12-10",
    "to_date": "2020-12-20",
    "users_order": "ascending",
    "criteria": {
        "sex": "male",
        "coverage": "center",
        "age": 35
    }
}
```



##### I. Example Response: User ranking in the center
```js
{
    "data": [
        {
            "user_uuid": "{{user_uuid}}",
            "place": 1,
            "attributes": [
                {
                    "variable": "steps",
                    "value": 3000
                },
                {
                    "variable": "trainings",
                    "value": 30
                }
            ]
        }
    ],
    "links": {
        "first": "{{base_url}}/rankings/users/list?from_date={{from_date}}&to_date={{to_date}}&ranking_type_uuid={{ranking_type_uuid}}&page=1",
        "last": "{{base_url}}/rankings/users/list?from_date={{from_date}}&to_date={{to_date}}&ranking_type_uuid={{ranking_type_uuid}}&page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "{{base_url}}/rankings/users/list",
        "per_page": 30,
        "to": 1,
        "total": 1
    }
}
```


***Status Code:*** 200

<br>



## Rewards

In this section you will find a list of the resources available in the RookMotion API to manage user rewards.



### 1. Add rewards to user



***Endpoint:***

```bash
Method: POST
Type: RAW
URL: https://api.rookmotion.com/v2/rewards/user
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |



***Body:***

```js        
{
    "rewards": [
        {
            "reward_type_uuid": "{{reward_type_uuid}}",
            "value": 0.1
        }
    ]
}
```



***More example Requests/Responses:***


##### I. Example Request: add reward training


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |



***Body:***

```js        
{
    "rewards": [
        {
            "reward_type_uuid": "{{reward_type_uuid}}",
            "value": 0.1
        }
    ]
}
```



##### I. Example Response: add reward training
```js
{
    "result": "added"
}
```


***Status Code:*** 201

<br>



### 2. Remove rewards from user



***Endpoint:***

```bash
Method: DELETE
Type: 
URL: https://api.rookmotion.com/v2/rewards/user
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| reward_uuid | RewardUuid | Unique reward identifier for RookMotion |
| user_uuid | UserUuid | Unique user identifier for RookMotion |



***More example Requests/Responses:***


##### I. Example Request: Remove rewards from the user


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| reward_uuid | RewardUuid | Unique reward identifier for RookMotion |
| user_uuid | UserUuid | Unique user identifier for Rook Motion |



***Status Code:*** 204

<br>



### 3. Retrive rewards of user



***Endpoint:***

```bash
Method: GET
Type: 
URL: https://api.rookmotion.com/v2/rewards/user
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |
| from_date | FromDate | Optional, initial end of period |
| to_date | ToDate | Optional, end end of period |



***More example Requests/Responses:***


##### I. Example Request: Retrive user rewards


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |
| from_date | FromDate | Optional, initial date of period |
| to_date | ToDate | Optional, end date of period |



##### I. Example Response: Retrive user rewards
```js
{
    "data": [
        {
            "reward_uuid": "{{reward_uuid}}",
            "reward_type_uuid": "{{reward_type_uuid}}",
            "value": 0.1
        }
    ],
    "links": {
        "first": "{{base_url}}/rewards/user/list?user_uuid={{user_uuid}}&from_date={{from_date}}&to_date={{to_date}}&page=1",
        "last": "{{base_url}}/rewards/user/list?user_uuid={{user_uuid}}&from_date={{from_date}}&to_date={{to_date}}&page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "{{base_url}}/rewards/user/list?user_uuid={{user_uuid}}",
        "per_page": 30,
        "to": 1,
        "total": 1
    }
}
```


***Status Code:*** 200

<br>



## Sensors
In this section you will find a list of the resources available in the RookMotion API to manage user sensors.



### 1. Add sensor to user



***Endpoint:***

```bash
Method: POST
Type: RAW
URL: https://api.rookmotion.com/v2/sensors/user
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |



***Body:***

```js        
{
    "name": "HW702A-582930",
    "mac": "C4:E5:50:7E:3F:7A"
}
```



***More example Requests/Responses:***


##### I. Example Request: Add sensors to user


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |



***Body:***

```js        
{
    "name": "HW702A-582930",
    "mac": "C4:E5:50:7E:3F:7A"
}
```



##### I. Example Response: Add sensors to user
```js
{
    "result": "added",
    "sensor_uuid": "{{sensor_uuid}}"
}
```


***Status Code:*** 201

<br>



### 2. Remove sensor from user



***Endpoint:***

```bash
Method: DELETE
Type: 
URL: https://api.rookmotion.com/v2/sensors/user/
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |
| sensor_uuid | SensorUuid | Unique sensor identifier for RookMotion |



***More example Requests/Responses:***


##### I. Example Request: Remove sensors from the user


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |
| sensor_uuid | SensorUuid | Unique sensor identifier for RookMotion |



***Status Code:*** 204

<br>



### 3. Retrive sensors from user



***Endpoint:***

```bash
Method: GET
Type: 
URL: https://api.rookmotion.com/v2/sensors/user
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |
| from_date | FromDate | Optional, initial end of period |
| to_date | ToDate | Optional, end date of period |



***More example Requests/Responses:***


##### I. Example Request: Retrive sensors from the user


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |
| from_date | FromDate | Optional, initial end of period |
| to_date | ToDate | Optional, end date of period |



##### I. Example Response: Retrive sensors from the user
```js
{
    "data": [
        {
            "sensor_uuid": "{{sensor_uuid}}",
            "attributes": {
                "name": "HW702A-582930",
                "mac": "C4:E5:50:7E:3F:7A"
            }
        }
    ],
    "links": {
        "first": "{{base_url}}/sensors/user/list?page=1",
        "last": "{{base_url}}/sensors/user/list?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "{{base_url}}/sensors/user/list/sensor",
        "per_page": 30,
        "to": 1,
        "total": 1
    }
}
```


***Status Code:*** 200

<br>



### 4. Update sensor to user



***Endpoint:***

```bash
Method: PUT
Type: RAW
URL: https://api.rookmotion.com/v2/sensors/user
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |
| sensor_uuid | SensorUuid | Unique sensor identifier for RookMotion |



***Body:***

```js        
{
    "name": "HW702A-582930",
    "mac": "C4:E5:50:7E:3F:7A"
}
```



***More example Requests/Responses:***


##### I. Example Request: Update sensor of the user


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |
| sensor_uuid | SensorUuid | Unique sensor identifier for RookMotion |



***Body:***

```js        
{
    "name": "HW702A-582930",
    "mac": "C4:E5:50:7E:3F:7A"
}
```



##### I. Example Response: Update sensor of the user
```js
{
    "result": "updated"
}
```


***Status Code:*** 200

<br>



## Server



### 1. Retrive server datetime



***Endpoint:***

```bash
Method: GET
Type: 
URL: https://api.rookmotion.com/v2/server/datetime
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***More example Requests/Responses:***


##### I. Example Request: Retrive server datetime


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



##### I. Example Response: Retrive server datetime
```js
{
    "datetime": "2020-12-11 20:59:59"
}
```


***Status Code:*** 200

<br>



## Trainings
In this section you will find a list of the resources available in the RookMotion API to manage user trainings.



### 1. Add summaries type



***Endpoint:***

```bash
Method: POST
Type: RAW
URL: https://api.rookmotion.com/v2/summaries
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Body:***

```js        
{
    "summary_name": "Calorias totales"
}
```



***More example Requests/Responses:***


##### I. Example Request: Add summaries type


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Body:***

```js        
{
    "summary_name": "cal_tot"
}
```



##### I. Example Response: Add summaries type
```js
{
    "summary_type_uuid": "nmlndsñsadfgdfsfdgdgf"
}
```


***Status Code:*** 201

<br>



### 2. Add training to user



***Endpoint:***

```bash
Method: POST
Type: RAW
URL: https://api.rookmotion.com/v2/trainings/user
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |



***Body:***

```js        
{
    "start":"2020-11-23 17:38:58",
    "stop":"2020-11-23 17:40:57",
    "sensor_uuid": "ksfmsmskdñdskldmslfkmfs",
    "training_type_uuid": "nmlndfnlsdksdñdñsñsadfgfg",
    "records": [
        {
          "table": "heart_rate",
          "data":[
                  {
                    "timestamp": "2020-20-20 20:20:20",
                    "heart_rate": 80,
                    "effort": 100,
                    "calories": 2000,
                    "heart_rate_variability": 2000
                  }, 
                  {
                    "timestamp": "2020-20-20 20:20:21",
                    "heart_rate": 81,
                    "effort": 99,
                    "calories": 2001,
                    "heart_rate_variability": 2000
                  } 
            ]
        },
        {
        "table": "steps",
        "data":[
                {
                  "timestamp": "2020-20-20 20:20:20",
                  "steps": 5000,
                  "cadence": 20
                }, 
                {
                  "timestamp": "2020-20-20 20:20:21",
                  "steps": 5001,
                  "cadence": 21
                }
          ]
      }
    ],
    "sumaries": [
        {
            "summary_type_uuid": "nmlndsñsadfgdf",
            "value":120
        },
        {
            "summary_type_uuid": "nmlndsñsadfgdf",
            "value":120
        },
        {
            "summary_type_uuid": "nmlndsñsadfgdf",
            "value":120
        }
    ],
    "rewards":[
        {
            "reward_type_uuid": "nmlndsñsadfgdf",
            "value":0.1
        },
        {
            "reward_type_uuid": "nmlndsñsadfgdf",
            "value":1.3
        },
        {
            "reward_type_uuid": "nmlndsñsadfgdf",
            "value":1
        }
    ]
}
```



***More example Requests/Responses:***


##### I. Example Request: Add training to user


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |



***Body:***

```js        
{
    "start":"2020-11-23 17:38:58",
    "stop":"2020-11-23 17:40:57",
    "sensor_uuid": "ksfmsmskdñdskldmslfkmfs",
    "training_type_uuid": "nmlndfnlsdksdñdñsñsadfgfg",
    "records": [
        {
          "table": "heart_rate",
          "data":[
                  {
                    "timestamp": "2020-20-20 20:20:20",
                    "heart_rate": 80,
                    "effort": 100,
                    "calories": 2000,
                    "heart_rate_variability": 2000
                  }, 
                  {
                    "timestamp": "2020-20-20 20:20:21",
                    "heart_rate": 81,
                    "effort": 99,
                    "calories": 2001,
                    "heart_rate_variability": 2000
                  } 
            ]
        },
        {
        "table": "steps",
        "data":[
                {
                  "timestamp": "2020-20-20 20:20:20",
                  "steps": 5000,
                  "cadence": 20
                }, 
                {
                  "timestamp": "2020-20-20 20:20:21",
                  "steps": 5001,
                  "cadence": 21
                }
          ]
      }
    ],
    "sumaries": [
        {
            "summary_type_uuid": "nmlndsñsadfgdf",
            "value":120
        },
        {
            "summary_type_uuid": "nmlndsñsadfgdf",
            "value":120
        },
        {
            "summary_type_uuid": "nmlndsñsadfgdf",
            "value":120
        }
    ],
    "rewards":[
        {
            "reward_type_uuid": "nmlndsñsadfgdf",
            "value":0.1
        },
        {
            "reward_type_uuid": "nmlndsñsadfgdf",
            "value":1.3
        },
        {
            "reward_type_uuid": "nmlndsñsadfgdf",
            "value":1
        }
    ]
}
```



##### I. Example Response: Add training to user
```js
{
    "result": "added"
}
```


***Status Code:*** 201

<br>



### 3. Add training type



***Endpoint:***

```bash
Method: POST
Type: RAW
URL: https://api.rookmotion.com/v2/trainings/type
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Body:***

```js        
{    
    "training_type":[
        {
            "training_name": "basketball",
            "language_code": "en"
        },
        {
            "training_name": "baloncesto",
            "language_code": "es"
        }
    ]
}
```



***More example Requests/Responses:***


##### I. Example Request: Add a training type


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Body:***

```js        
{    
    "training_type":[
        {
            "training_name": "basketball",
            "language_code": "en"
        },
        {
            "training_name": "baloncesto",
            "language_code": "es"
        }
    ]
}
```



##### I. Example Response: Add a training type
```js
{
    "training_type_uuid": "{{training_type_uuid}}"
}
```


***Status Code:*** 201

<br>



### 4. Remove summaries type



***Endpoint:***

```bash
Method: DELETE
Type: 
URL: https://api.rookmotion.com/v2/summaries
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| summary_type_uuid | SummaryTypeUuid |  |



***More example Requests/Responses:***


##### I. Example Request: Remove summaries type


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| summary_type_uuid | SummaryTypeUuid |  |



***Status Code:*** 204

<br>



### 5. Remove training from user



***Endpoint:***

```bash
Method: DELETE
Type: 
URL: https://api.rookmotion.com/v2/trainings/user
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| training_uuid | trainingUuid | Unique training identifier for Rook Motion |
| user_uuid | UserUuid | Unique user identifier for RookMotion |



***More example Requests/Responses:***


##### I. Example Request: Remove training


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| training_uuid | trainingUuid | Unique training identifier for Rook Motion |
| user_uuid | UserUuid | Unique user identifier for RookMotion |



***Status Code:*** 204

<br>



### 6. Remove training type



***Endpoint:***

```bash
Method: DELETE
Type: 
URL: https://api.rookmotion.com/v2/trainings/type
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| training_type_uuid | TrainingTypeUuid | Unique training type identifier for RookMotion |



***More example Requests/Responses:***


##### I. Example Request: Remove training type


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| training_type_uuid | TrainingTypeUuid | Unique training type identifier for RookMotion |



***Status Code:*** 204

<br>



### 7. Retrieve training information of user



***Endpoint:***

```bash
Method: GET
Type: 
URL: https://api.rookmotion.com/v2/trainings/user
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| training_uuid | trainingUuid | Unique training identifier for Rook Motion |
| user_uuid | UserUuid | Unique user identifier for RookMotion |



***More example Requests/Responses:***


##### I. Example Request: Retrieve information about the user's training


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| training_uuid | trainingUuid | Unique training identifier for Rook Motion |
| user_uuid | UserUuid | Unique user identifier for RookMotion |



##### I. Example Response: Retrieve information about the user's training
```js
{
    "start": "2020-11-23 17:38:58",
    "stop": "2020-11-23 17:40:57",
    "sensor_uuid": "{{sensor_uuid}}",
    "training_type_label": "Box",
    "origin": {
          "device_type": "decoder",
          "client_name": "RookCompany",
          "client_center": "RookCompanyElite",
          "client_branch": "Condesa",
          "client_room": "Box"
    },
    "records": [
        {
            "table": "heart_rate",
            "data": [
                {
                    "timestamp": "2020-20-20 20:20:20",
                    "heart_rate": 80,
                    "effort": 100,
                    "calories": 2000,
                    "heart_rate_variability": 2000
                },
                {
                    "timestamp": "2020-20-20 20:20:21",
                    "heart_rate": 81,
                    "effort": 99,
                    "calories": 2001,
                    "heart_rate_variability": 2000
                }
            ]
        },
        {
            "table": "steps",
            "data": [
                {
                    "timestamp": "2020-20-20 20:20:20",
                    "steps": 5000,
                    "cadence": 20
                },
                {
                    "timestamp": "2020-20-20 20:20:21",
                    "steps": 5001,
                    "cadence": 21
                }
            ]
        }
    ],
    "sumaries": [
        {
            "summary_type_uuid": "{{summary_type_uuid}}",
            "value": 120
        }
    ],
    "rewards": [
        {
            "reward_type_uuid": "{{reward_type_uuid}}",
            "value": 0.1
        }
    ]
}
```


***Status Code:*** 200

<br>



### 8. Retrive summaries types



***Endpoint:***

```bash
Method: GET
Type: 
URL: https://api.rookmotion.com/v2/summaries
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| from_date | FromDate | Optional, initial end of period |
| to_date | ToDate | Optional, end date of period |
| page | 1 |  |



***More example Requests/Responses:***


##### I. Example Request: Retrive summaries type


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| from_date | FromDate | Optional, initial end of period |
| to_date | ToDate | Optional, end date of period |
| page | 1 |  |



##### I. Example Response: Retrive summaries type
```js
{
    "data": [
        {
            "summary_type_uuid": "{{summary_type_uuid}}",
            "attributes": [
                {
                    "summary_name": "avg_cal"
                }
            ]
        }
    ],
    "links": {
        "first": "{{base_url}}/summaries/list?from_date={{from_date}}&to_date={{to_date}}&page=1",
        "last": "{{base_url}}/summaries/list?from_date={{from_date}}&to_date={{to_date}}&page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "{{base_url}}/summaries/list",
        "per_page": 30,
        "to": 1,
        "total": 1
    }
}
```


***Status Code:*** 200

<br>



### 9. Retrive training state of user



***Endpoint:***

```bash
Method: GET
Type: 
URL: https://api.rookmotion.com/v2/trainings/user/status
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for Rook Motion |
| training_uuid | trainingUuid | Unique training identifier for RookMotion |



***More example Requests/Responses:***


##### I. Example Request: Retrive user training state


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user  identifier for Rook Motion |
| training_uuid | trainingUuid | Unique training identifier for RookMotion |



##### I. Example Response: Retrive user training state
```js
{
    "status": 1,
    "updated_at": "2020-10-27 11:36:47"    
}
```


***Status Code:*** 200

<br>



### 10. Retrive training types



***Endpoint:***

```bash
Method: GET
Type: 
URL: https://api.rookmotion.com/v2/trainings/types
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |
| Accept-Language | es |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| page | 1 |  |



***More example Requests/Responses:***


##### I. Example Request: Retrive the types of training


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |
| Accept-Language | es |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| page | 1 |  |



##### I. Example Response: Retrive the types of training
```js
{
    "data": [
        {
            "trainig_type_uuid": "{{trainig_type_uuid}}",
            "attributes": {
                "training_name": "basketball"
            }
        }
    ],
    "links": {
        "first": "{{base_url}}/trainings/types/list?page=1",
        "last": "{{base_url}}/trainings/types/list?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "{{base_url}}/v2/training/types/list",
        "per_page": 30,
        "to": 1,
        "total": 1
    }
}
```


***Status Code:*** 200

<br>



### 11. Retrive trainings of user



***Endpoint:***

```bash
Method: GET
Type: 
URL: https://api.rookmotion.com/v2/trainings/user
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| page | 1 |  |
| from_date | FromDate | Optional, initial end of period |
| to_date | ToDate | Optional, end date of period |
| user_uuid | UserUuid | Unique user identifier for RookMotion |



***More example Requests/Responses:***


##### I. Example Request: List user trainings


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| page | 1 |  |
| from_date | FromDate | Optional, initial end of period |
| to_date | ToDate | Optional, end date of period |
| user_uuid | UserUuid | Unique user identifier for RookMotion |



##### I. Example Response: List user trainings
```js
{
    "data": [
        {
            "training_uuid": "{{training_uuid}}",
            "attributes": {
                "start": "2020-09-18 22:12:16",
                "stop": "2020-09-18 22:17:15",
                "duration": 299,
                "training_type_label": "Box",
                "origin": {
                    "device_type": "decoder",
                    "client_name": "RookCompany",
                    "client_center": "RookCompanyElite",
                    "client_branch": "Condesa",
                    "client_room": "Box"
                },
                "summaries": {
                    "calories": 1000
                }
            }
        }
    ],
    "links": {
        "first": "{{base_url}}/trainings/user/list?page=1&from_date={{from_date}}&to_date={{to_date}}&user_uuid={{user_uuid}}",
        "last": "{{base_url}}/trainings/user/list?page=1&from_date={{from_date}}&to_date={{to_date}}&user_uuid={{user_uuid}}",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "{{base_url}}/trainings/user/list",
        "per_page": 30,
        "to": 1,
        "total": 1
    }
}
```


***Status Code:*** 200

<br>



### 12. Update summaries type



***Endpoint:***

```bash
Method: PUT
Type: RAW
URL: https://api.rookmotion.com/v2/summaries
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| summary_type_uuid | SummaryTypeUuid |  |



***Body:***

```js        
{
    "summary_name": "avg_cal"
}
```



***More example Requests/Responses:***


##### I. Example Request: Update summaries type


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| summary_type_uuid | SummaryTypeUuid |  |



***Body:***

```js        
{
    "summary_name": "avg_cal"
}
```



##### I. Example Response: Update summaries type
```js
{
    "result": "updated"
}
```


***Status Code:*** 200

<br>



### 13. Update training type



***Endpoint:***

```bash
Method: PUT
Type: RAW
URL: https://api.rookmotion.com/v2/trainings/type
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| training_type_uuid | TrainingTypeUuid | Unique training type identifier for RookMotion |



***Body:***

```js        
{
    "training_name": "basketball",
    "language_code": "en"
}
```



***More example Requests/Responses:***


##### I. Example Request: Update training type


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| training_type_uuid | TrainingTypeUuid | Unique training type identifier for RookMotion |



***Body:***

```js        
{
    "training_name": "basketball"
}
```



##### I. Example Response: Update training type
```js
{
    "result": "updated"
}
```


***Status Code:*** 200

<br>



## Users
In this section you will find a list of the resources available in the RookMotion API to manage users.



### 1. Add user physiological variables



***Endpoint:***

```bash
Method: POST
Type: RAW
URL: https://api.rookmotion.com/v2/users/variables
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |



***Body:***

```js        
{
    "weight": 70,
    "height": 170,
    "resting_heart_rate": 60
}
```



***More example Requests/Responses:***


##### I. Example Request: Add user physiological variables


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |



***Body:***

```js        
{
    "weight": 70,
    "height": 170,
    "resting_heart_rate": 60
}
```



##### I. Example Response: Add user physiological variables
```js
{
    "result": "added"
}
```


***Status Code:*** 201

<br>



### 2. Add user to RookMotion



***Endpoint:***

```bash
Method: POST
Type: RAW
URL: https://api.rookmotion.com/v2/users
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| client_key | Identifier of the branches, couch, rooms, etc. |  |
| Content-Type | application/json |  |



***Body:***

```js        
{
    "email": "{{email}}"
}
```



***More example Requests/Responses:***


##### I. Example Request: Add user to RookMotion


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| client_key | Identifier of the branches, couch, rooms, etc. |  |
| Content-Type | application/json |  |



***Body:***

```js        
{
    "email": "{{email}}",
    "user_membership_id": "{{user_membership_id}}"
}
```



##### I. Example Response: Add user to RookMotion
```js
{
    "user_uuid": "{{user_uuid}}"
}
```


***Status Code:*** 201

<br>



### 3. Remove user for RookMotion



***Endpoint:***

```bash
Method: DELETE
Type: 
URL: https://api.rookmotion.com/v2/users
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |



***More example Requests/Responses:***


##### I. Example Request: Remove user for RookMotion


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |



***Status Code:*** 204

<br>



### 4. Retrieve user physiological variables



***Endpoint:***

```bash
Method: GET
Type: 
URL: https://api.rookmotion.com/v2/users/variables
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |
| from_date | FromDate | Optional, initial end of period |
| to_date | ToDate | Optional, end date of period |



***More example Requests/Responses:***


##### I. Example Request: Retrieve user physiological variables


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |
| from_date | FromDate | Optional, initial end of period |
| to_date | ToDate | Optional, end date of period |



##### I. Example Response: Retrieve user physiological variables
```js
{
    "data": [
        {
            "datetime": "2020-10-10 10:10:10",
            "attributes": {
                "weight": 75,
                "height": 160,
                "resting_heart_rate": 60
            }
        },
        {
            "datetime": "2021-10-10 10:10:10",
            "attributes": {
                "weight": 80,
                "height": 160,
                "resting_heart_rate": 60
            }
        }
    ],
    "links": {
        "first": "{{base_url}}/users/variables/list?page=1",
        "last": "{{base_url}}/users/variables/list?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "{{base_url}}/users/variables/list",
        "per_page": 30,
        "to": 1,
        "total": 2
    }
}
```


***Status Code:*** 200

<br>



### 5. Retrieve user uuid



***Endpoint:***

```bash
Method: GET
Type: 
URL: https://api.rookmotion.com/v2/users/uuid
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| email | EmailUser | User email |



***More example Requests/Responses:***


##### I. Example Request: Retrieve user uuid


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| email | EmailUser | User email |



##### I. Example Response: Retrieve user uuid
```js
{
    "user_uuid": "{{user_uuid}}"
}
```


***Status Code:*** 200

<br>



### 6. Retrive client users



***Endpoint:***

```bash
Method: GET
Type: 
URL: https://api.rookmotion.com/v2/client/users
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| page | 1 |  |



***More example Requests/Responses:***


##### I. Example Request: Retrive the users of the center


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| page | 1 |  |



##### I. Example Response: Retrive the users of the center
```js
{
    "data": [
        {
            "user_uuid": "{{user_uuid}}",
            "attributes": {
                "email": "{{email}}",
                "user_membership_id": "ABCD1234",
                "name": "a name",
                "sex": "male",
                "birthday": "1991-07-29",
                "pseudonym": "tom"
            }
        }
    ],
    "links": {
        "first": "{{base_url}}/client/users/list?page=1",
        "last": "{{base_url}}/client/users/list?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "{{base_url}}/client/users",
        "per_page": 30,
        "to": 1,
        "total": 1
    }
}
```


***Status Code:*** 200

<br>



### 7. Retrive user indexes



***Endpoint:***

```bash
Method: GET
Type: 
URL: https://api.rookmotion.com/v2/users/indexes/
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |



***More example Requests/Responses:***


##### I. Example Request: Retrive indexes of the user


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |



##### I. Example Response: Retrive indexes of the user
```js
{
    "attributes": {
        "A": 1.1,
        "B": 1.2,
        "C": 1.4
    }
}
```


***Status Code:*** 200

<br>



### 8. Retrive user information



***Endpoint:***

```bash
Method: GET
Type: 
URL: https://api.rookmotion.com/v2/users/info
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |



***More example Requests/Responses:***


##### I. Example Request: Retrive user information


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |



##### I. Example Response: Retrive user information
```js
{
    "user_uuid": "{{user_uuid}}",
    "attributes": {
        "email": "{{email}}",
        "user_membership_id": "{{user_membership_id}}",
        "name": "user_name",
        "sex": "male",
        "birthday": "1991-07-29",
        "pseudonym": "John"
    }
}
```


***Status Code:*** 200

<br>



### 9. Retrive user statistics



***Endpoint:***

```bash
Method: GET
Type: 
URL: https://api.rookmotion.com/v2/users/training/compare
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |
| from_date | FromDate | Optional, initial end of period |
| to_date | ToDate | Optional, end date of period |



***More example Requests/Responses:***


##### I. Example Request: Retrive user statistics


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |
| from_date | FromDate | Optional, initial end of period |
| to_date | ToDate | Optional, end date of period |



##### I. Example Response: Retrive user statistics
```js
{
    "last_period": {
        "from_date": "2020-01-01",
        "to_date": "2020-01-31",
        "trainings": 10,
        "calories": 800,
        "steps": 60000,
        "weight": 64,
        "rewards": 700
    },
    "current_period": {
        "from_date": "2020-02-01",
        "to_date": "2020-02-28",
        "trainings": 10,
        "calories": 10,
        "steps": 50000,
        "weight": 64,
        "rewards": 800
    }
}
```


***Status Code:*** 200

<br>



### 10. Retrive user status



***Endpoint:***

```bash
Method: GET
Type: 
URL: https://api.rookmotion.com/v2/users/status
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |



***More example Requests/Responses:***


##### I. Example Request: Retrive user status


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |



##### I. Example Response: Retrive user status
```js
{
    "status": 1
}
```


***Status Code:*** 200

<br>



### 11. Set user membership



***Endpoint:***

```bash
Method: POST
Type: RAW
URL: https://api.rookmotion.com/v2/users/memberships
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| client_key | Identifier of the branches, couch, rooms, etc. |  |
| Content-Type | application/json |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |



***Body:***

```js        
{
    "membership": "{{user_membership_id}}"
}
```



***More example Requests/Responses:***


##### I. Example Request: Add membership user


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| client_key | Identifier of the branches, couch, rooms, etc. |  |
| Content-Type | application/json |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |



***Body:***

```js        
{
    "membership": "{{user_membership_id}}"
}
```



##### I. Example Response: Add membership user
```js
{
    "result": "added"
}
```


***Status Code:*** 201

<br>



### 12. Update user information



***Endpoint:***

```bash
Method: PUT
Type: RAW
URL: https://api.rookmotion.com/v2/users/info
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |



***Body:***

```js        
{
    "name": "Jhon Rook",
    "sex": "male",
    "birthday": "1991-07-29",
    "pseudonym": "Jhon"
}
```



***More example Requests/Responses:***


##### I. Example Request: Update user information


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |



***Query:***

| Key | Value | Description |
| --- | ------|-------------|
| user_uuid | UserUuid | Unique user identifier for RookMotion |



***Body:***

```js        
{
    "name": "Jhon Rook",
    "sex": "male",
    "birthday": "1991-07-29",
    "pseudonym": "Jhon"
}
```



##### I. Example Response: Update user information
```js
{
    "result": "updated"
}
```


***Status Code:*** 200

<br>



***Available Variables:***

| Key | Value | Type |
| --- | ------|-------------|
| base_url | https://api.rookmotion.com/v2 |  |
| client_key | Identifier of the branches, couch, rooms, etc. |  |
| Insert_your_username_here | Insert your username here |  |
| Insert_your_password_here | Insert your password here |  |
| content_type | application/json |  |
| email | EmailUser |  |
| user_membership_id | UserMembershipId |  |
| user_uuid | UserUuid |  |
| from_date | FromDate |  |
| to_date | ToDate |  |
| sensor_uuid | SensorUuid |  |
| training_type_uuid | TrainingTypeUuid |  |
| training_uuid | trainingUuid |  |
| reward_uuid | RewardUuid |  |
| ranking_type_uuid | RankingTypeUuid |  |
| summary_type_uuid | SummaryTypeUuid |  |
| one_of_the_clients_key | Identifier of the branches, couch, rooms, etc. |  |



---
[Back to top](#rookmotion-api-v200)
> Made with &#9829; by [thedevsaddam](https://github.com/thedevsaddam) | Generated at: 2021-03-01 14:45:11 by [docgen](https://github.com/thedevsaddam/docgen)
